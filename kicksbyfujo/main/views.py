from flask import render_template,redirect,Blueprint
from kicksbyfujo.main.forms import LoginForm
home = Blueprint('home', __name__)

@home.route('/')
@home.route('/index')
def index():
    return render_template("index.html")
@home.route('/login')
def login():
    form = LoginForm()
    return render_template('contact.html',form=form)