from flask import Flask
from flask_assets import Environment, Bundle
from config import Config
from kicksbyfujo.main.views import home


app = Flask(__name__)
app.config.from_object(Config)
app.config['ASSETS_DEBUG'] = True

bundles = {
    'home_js': Bundle(
        'js/jquery.js',
        'js/jquery.smartmenus.min.js',
        'js/queryloader2.min.js',
        'js/jquery.carouFredSel-6.0.0-packed.js',
        'js/jquery.mousewheel.min.js',
        'js/jquery.touchSwipe.min.js',
        'js/jquery.easing.1.3.js',
        'js/jquery.nicescroll.min.js',
        'js/main.js',
        filters='jsmin',
        output='gen/home.js'
    ),
    'home_css': Bundle(
        'css/carouFredSel.css',
        'css/clear.css',
        'css/common.css',
        'css/font-awesome.min.css',
        'css/sm-clean.css',
        'style.css',
        filters='cssmin',
        output='gen/home.css'
    )

}
assets = Environment(app)

assets.register(bundles)

app.register_blueprint(home)
